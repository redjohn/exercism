public class Combiner {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;
        Combiner combiner = new Combiner(10);
        System.out.println(combiner.combine(x, y));
    }

    private int value = 0;

    public Combiner(int value) {
        this.value = value;
    }

    public int combine(int first,  int second) {
        return first + second + value;
    }
}

