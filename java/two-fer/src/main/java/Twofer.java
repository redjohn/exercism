public class Twofer {
    public String twofer(String name) {
        String defaultName = "you";
        if (name == null) {
            name = defaultName;
        }
        return "One for " + name + ", one for me.";
    }
}