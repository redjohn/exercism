public class Testing {
    public static void main(String[] args) {
        int times = 10;
        if (args.length > 0) {
            times = Integer.parseInt(args[0]);
        }
        String name = "Alice";
        String friendName = "Bob";

        System.out.println(greet(name));
        System.out.println(greet(friendName));

        greetMultiple(times, friendName);
    }

    public static String greet(String name) {
        if (name.equals("Bob")) {
            return "Go away Bob";
        } else {
            return "Hello " + name + "!";
        }
    }

    public static void greetMultiple(int times, String name) {
        for (int i = 0; i < times; ++i) {
           System.out.println(greet(name));
        }
    }
}
