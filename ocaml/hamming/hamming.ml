open Core

type nucleotide = A | C | G | T

let hamming_distance xs ys =
  let diff total x y = if x == y then total else total + 1 in
  Option.try_with @@ fun () -> List.fold2_exn xs ys ~init:0 ~f:diff
