open Core

let sum_of_squares x =
  x
  |> List.range ~stop:`inclusive 1
  |> List.map ~f:(fun x -> x * x)
  |> List.fold ~init:0 ~f:(+)


let square_of_sum x =
  x
  |> List.range ~stop:`inclusive 1
  |> List.fold ~init:0 ~f:(+)
  |> fun x -> x* x

let difference_of_squares x = square_of_sum x - sum_of_squares x
