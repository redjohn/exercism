open Core

let make_sound factor sound num = if num mod factor = 0 then Some sound else None

let pling = make_sound 3 "Pling"

let plang = make_sound 5 "Plang"

let plong = make_sound 7 "Plong"

let raindrop x =
  let default = string_of_int x in
  let combine first second = match (first, second) with
    | (Some val1, Some val2) -> Some (val1 ^ val2)
    | _ -> Option.first_some first second
  in
  let sounds = List.map [pling; plang; plong] ~f:(fun f -> f x) in
  let combined = List.fold sounds ~init:None ~f:combine in
  Option.value combined ~default
