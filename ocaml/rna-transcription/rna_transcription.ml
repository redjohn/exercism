open Core

type dna = [ `A | `C | `G | `T ]
type rna = [ `A | `C | `G | `U ]

let map_dna dna_base = match dna_base with
  | `A -> `U
  | `T -> `A
  | `G -> `C
  | `C -> `G

let to_rna dna_strand = (List.map dna_strand ~f:map_dna)
